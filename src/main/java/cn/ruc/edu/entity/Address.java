package cn.ruc.edu.entity;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Address implements Serializable {
    public final String UUID;

    private String name;

    private String nickName;

    private String phone;

    private String address;

    public Address(String UUID, String name, String nickName, String phone, String address) {
        this.UUID = UUID;
        this.name = name;
        this.nickName = nickName;
        this.phone = phone;
        this.address = address;
    }

    public Address(String UUID) {
        this.UUID = UUID;
    }

    private static final File file = new File("./store.bin");

    public static boolean save(Address... address) {

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = null;
            try {
                oos = new ObjectOutputStream(fos);
                oos.writeObject(address);            //写入对象
                oos.flush();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } finally {
                try {
                    if (oos != null) {
                        oos.close();
                    }
                } catch (IOException e) {
                    System.out.println("oos关闭失败：" + e.getMessage());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("找不到文件：" + e.getMessage());
            return false;
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                System.out.println("fos关闭失败：" + e.getMessage());
            }
        }
        return true;
    }

    public static Address[] read() {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(fis);
                try {
                    return (Address[]) ois.readObject();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (ois != null) {
                        ois.close();
                    }
                } catch (IOException e) {
                    System.out.println("ois关闭失败：" + e.getMessage());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("找不到文件：" + e.getMessage());
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                System.out.println("fis关闭失败：" + e.getMessage());
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return UUID.equals(address.UUID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(UUID);
    }

    @Override
    public String toString() {
        return "Address{" +
                "UUID='" + UUID + '\'' +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Address address = new Address("a", "b", "c", "d", "e");
        Address.save(address);
        Address[] newer = Address.read();
        for (Address d : newer) {
            System.out.println(d);
        }
    }
}
