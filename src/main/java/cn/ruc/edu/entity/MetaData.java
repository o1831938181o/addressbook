package cn.ruc.edu.entity;

import java.io.*;
import java.util.Date;

/**
 * <a href="https://www.cnblogs.com/didispace/p/16263870.html">record</a>
 * <p>
 * <p>
 * auto generate equals-hashcode-toString
 * auto generate get method
 *
 * @param createDate
 * @param modifyDate
 */
public record MetaData(Date createDate, Date modifyDate) implements Serializable {
    public MetaData(Date createDate) {
        this(createDate, null);
    }

    private static final File file = new File("./meta.bin");

    public static boolean save(MetaData metaData) {

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = null;
            try {
                oos = new ObjectOutputStream(fos);
                oos.writeObject(metaData);            //写入对象
                oos.flush();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } finally {
                try {
                    if (oos != null) {
                        oos.close();
                    }
                } catch (IOException e) {
                    System.out.println("oos关闭失败：" + e.getMessage());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("找不到文件：" + e.getMessage());
            return false;
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                System.out.println("fos关闭失败：" + e.getMessage());
            }
        }
        return true;
    }

    public static MetaData read() {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(fis);
                try {
                    return (MetaData) ois.readObject();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (ois != null) {
                        ois.close();
                    }
                } catch (IOException e) {
                    System.out.println("ois关闭失败：" + e.getMessage());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("找不到文件：" + e.getMessage());
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                System.out.println("fis关闭失败：" + e.getMessage());
            }
        }
        return null;
    }
}
