package cn.ruc.edu;

import cn.ruc.edu.entity.Address;
import cn.ruc.edu.entity.MetaData;
import cn.ruc.edu.exec.AddressManager;
import cn.ruc.edu.util.UUIDUtils;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        AddressManager manager = AddressManager.getAddressManager();

        String specUUID = "39ebce0865904ae3aeea8363ca7069db";
        manager.add(new Address(specUUID, "chenpeigen", "peter", "18079919252", "beijin"));
        manager.delete(new Address(specUUID));
        manager.add(new Address(specUUID, "chenpeigen", "peter", "18079919252", "beijin"));
        Address address = manager.select(specUUID);
        System.out.println(address);
        manager.update(new Address(specUUID, "chenpeigen1", "peter1", "18079919252", "beijin"));
        address = manager.select(specUUID);
        System.out.println(address);

        Thread.sleep(1000L); // sleep delay commit

        boolean flag = manager.commit(); // commit add update delete manipulate into the bin
        System.out.println(flag);


        MetaData metaData = manager.getMetaData();
        System.out.println(metaData.createDate());
        System.out.println(metaData.modifyDate());
    }
}