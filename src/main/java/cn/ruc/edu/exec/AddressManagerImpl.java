package cn.ruc.edu.exec;

import cn.ruc.edu.entity.Address;
import cn.ruc.edu.entity.MetaData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class AddressManagerImpl implements AddressManager {

    private final List<Address> addresses;

    private MetaData metaData;

    public AddressManagerImpl(Address... address) {
        if (address == null) addresses = new ArrayList<>();
        else addresses = new ArrayList<>(List.of(address));
        MetaData metaData = MetaData.read();
        if (metaData == null) metaData = new MetaData(new Date());
        this.metaData = metaData;
    }

    @Override
    public AddressManager add(Address address) {
        addresses.add(address);
        return this;
    }

    @Override
    public AddressManager delete(Address address) {
        addresses.remove(address);
        return this;
    }

    @Override
    public AddressManager update(Address address) {
        addresses.remove(address);
        addresses.add(address);
        return this;
    }

    @Override
    public Address select(String uuid) {
        for (Address address : addresses) {
            if (address.UUID.equals(uuid)) return address;
        }
        return null;
    }

    @Override
    public MetaData getMetaData() {
        return metaData;
    }

    @Override
    public boolean commit() {
        Address[] addArray = new Address[addresses.size()];
        MetaData writeMetaData = new MetaData(metaData.createDate(), new Date());
        this.metaData = writeMetaData;
        MetaData.save(writeMetaData);
        return Address.save(addresses.toArray(addArray));
    }
}
