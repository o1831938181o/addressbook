package cn.ruc.edu.exec;

import cn.ruc.edu.entity.Address;
import cn.ruc.edu.entity.MetaData;

/**
 * manager for address
 */
public interface AddressManager {
    AddressManager add(Address address);

    AddressManager delete(Address address);

    AddressManager update(Address address);

    Address select(String uuid);

    MetaData getMetaData();

    boolean commit();

    static AddressManager getAddressManager() {
        return new AddressManagerImpl(Address.read());
    }
}
