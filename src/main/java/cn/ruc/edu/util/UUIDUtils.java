package cn.ruc.edu.util;

import java.util.*;

public final class UUIDUtils {

    private UUIDUtils() {}

    public static String getUUID32() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }

}
